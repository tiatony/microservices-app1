# Microservices - app1

This app is part of the Microservices project.
app1 listens and consumes messages from the queue sent by app2.
For this, app1 has a StatusUpdate message and a StatusUpdateHandler handler, in the same format as in the app2. 

## Setup
See Microservices repository documentation : https://gitlab.com/tiatony/microservices